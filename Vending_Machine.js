readline = require('readline-sync');

var colors = require('colors');
colors.setTheme({
  custom: ['red', 'underline']
});

var vendor = {
  // balance and items stored as global variables so that they can be accessed anywhere in the code
  Funds: 0.00,



  //All vendor products are stored as items, so each can be referenced.
  //Quantity can be deduced per order, so that the user knows if its sold out
  items: [{
      drinkBrand: 'Coca-Cola ',
      cost: 1.99,
      Quantity: 10
    },
    {
      drinkBrand: 'Dr. Pepper',
      cost: 1.99,
      Quantity: 10
    },
    {
      drinkBrand: 'Fanta     ',
      cost: 1.99,
      Quantity: 10
    },
    {
      drinkBrand: 'Rio       ',
      cost: 1.65,
      Quantity: 10
    },
    {
      drinkBrand: 'Ribena    ',
      cost: 1.59,
      Quantity: 10
    },
    {
      drinkBrand: 'Water     ',
      cost: 1.25,
      Quantity: 10
    },

  ]
};



//Start function
function main_menu() {
  //clear console clears the console, to decrease confusion for the user
  //colour added for aesthetics, appealing to users
  //toFixed is used to keep funds to 2 d.p(Ducket, 2014).
  console.clear();
  console.log("                           ".bgCyan);
  console.log("      Drinks Vendor        ".bold.bgCyan);
  console.log("     Joseph Percival       ".bold.bgCyan);
  console.log("                           ".bgCyan);
  console.log("                           ".bgCyan);
  console.log("  Please make a selection: ".bold.bgCyan.blue);
  console.log("                           ".bgCyan);
  console.log("    1. Select a drink      ".bold.bgCyan);
  console.log("    2. View your balance   ".bold.bgCyan);
  console.log("    3. Add Money           ".bold.bgCyan);
  console.log("    4. Refund Money        ".bold.bgCyan);
  console.log("    5. Exit                ".bold.bgCyan);
  console.log("                           ".bgCyan);


  /*Requests the readline question module so that the user can input their selection*/
  var selection = readline.question(" Please select an option:".bold);

  /*The Switch statement executes the following block of code containing the different cases*/
  /*These cases are conditional statements that the user selects to perform diffrent actions(W3C, 2018)*/

  switch (parseInt(selection)) {
    case 1:
      chooseDrink();
      break;
    case 2:
      creditCheck();
      break;
    case 3:
      addMoney();
      break;
    case 4:
      refundMoney();
      break;
      /*if the user selects option 5 "Exit" the program ends, "breaks" the program*/
    case 5:
      break;
    default:
      /*Error prevention. If the user selects a number outside of the selection range*/
      /*or they accidently press an invalid key as a selction, then this section in invoked and procedures follow*/
      /*To get the users attention the colour Yellow (Warning) was added to the text using the,"colors" global var*/
      console.clear();
      console.log("Sorry, that was not a valid selection".yellow.custom);
      console.log("Press Enter to return to the main menu".yellow)
      readline.question();
      main_menu();
      break;
  }
}
/*end of function*/

/*start drinks selection function*/
function chooseDrink() {
  /*global variable items is called, invoking the setting  */
  var items = vendor.items;

  console.clear();
  /*toFixed(2) added for cost reasons-keeps decimal places(pennies) to 2 d.p)*/
  console.log("                                              ");
  console.log("      Selct your option              ".cyan);
  console.log("                                              ");
  console.log("  Your Current Balance: £" + vendor.Funds.toFixed(2) + "               ");
  console.log("                                             ");

  /*The for statement loops through every property of the object, the block of code gets executed at least once, for each property*/
  for (var i = 0; i < items.length; i++) {
    var Drink = items[i];

    console.log(colors.inverse.blue(i + ". " + Drink.drinkBrand + " - " + ": £" + Drink.cost + " - " + Drink.Quantity + " Funds"));
    /*The drinkBrand specificities are displayed as a list with "i" used as the beginning of the list*/
    /*Additional effects, of inversing the color Blue was used, to highlight this area of interest to the user*/
    /*The reason behind this was to identify the locations of these in the index*/
  }

  console.log("                       ");
  var selection = readline.question("    Please make a selection: ".cyan);
  /*Where NaN(Not a Number) is concerned, this is regarded as false*/
  if (isNaN(selection) == false) {
    /*parseInt() will take the key the user pressed,as a string, and turns it into a number.*/
    var selectionAsNumber = parseInt(selection);
    /*If the number entered is >= 0 the program moves to phase B */
    /*The && operator compares 2 experiments, rturns false unless both results are each true (Anon, 2018)*/
    if (selectionAsNumber >= 0 && selectionAsNumber < items.length) {
      // the program then passes on to the phaseb
      phaseb_purchase(selectionAsNumber);
    } else {
      //Error Prevention & recovery: if an incorrect number is selected, the followig  occur
      console.log("Sorry the number entered was not on the above list");
      console.log("Please choose a number from above - press any key to continue".cyan);
      readline.question();
      chooseDrink(items);
    }
  } else {
    //*Error Prevention & recovery: If the user enters something that is not a number(a letter)
    console.log("Sorry, that was not a number, please press the enter key and try again".cyan);
    readline.question();
    chooseDrink(items);
  }
}

// End function
//End of Phase A

//Phase B

//Start function

function phaseb_purchase(Drink_index) {
  console.clear();
  // New Drink variable to store the Drinks index, it retrieves fom the items object in the Vendor global
  var Drink = vendor.items[Drink_index];

  // prints the selection s name, cost and how many remain
  console.log(Drink.drinkBrand + Drink.cost);

  //This handles situations where the user doesnt have enough funds and wht to do to rectify this situation
  //Yellow and green colours are deployed to alert the user of a money(green) warning(yellow)
  if (Drink.cost > vendor.Funds) {
    console.clear();
    console.log("Sorry, you do not have enough funds".yellow)
    console.log(colors.green("Your current balance is: £" + vendor.Funds.toFixed(2)))
    console.log("Please press enter to return to the main menu ".yellow)
    readline.question();
    main_menu();

  } else {
    // This if statement handles situations if an item is Sold Out
    if (Drink.Funds == 0) {
      console.clear();
      console.log("Sold Out".cyan)
      console.log("Please press any key to return to the main menu".white)
      readline.question();
      main_menu();
    }
    // This section handles a situation of a Point of Sale. Displays Item and its cost
    // Subtracts the cost from the users balance, displays this as green (money) thankyou messsage in yellow
    // Return to main menu in Cyan to complement the previous colours.
    console.clear()
    vendor.Funds -= Drink.cost.toFixed(2);
    Drink.Funds -= 1;
    console.log(colors.magenta("     You have bought: " + Drink.drinkBrand + " Price: £" + Drink.cost));
    console.log(colors.yellow("            Thankyou and come again!"));
    console.log("             Your balance is: " + "£" + vendor.Funds.toFixed(2));
    console.log("   Press any key to return to the main menu".cyan);
    readline.question();
    main_menu();
  }
}
/*End of stage 2*/

/*start of function*/

function addMoney() {
  console.clear();
  console.log("Please enter credit in numbers");
  console.log("    Or press enter to exit    ");
  /*User input is recorded into this variable as a string with the currency symbol to start*/
  var selection = readline.question("           £");

  /*selectionAsFloat recieves the input from selection and parses it as a float*/
  var selectionAsFloat = parseFloat(selection);

  // If the input is not a number it return as false, and if the input is greater than 0 it updates the Funds global variable
  // Nan checks if an input is Not a Number (Nan) (Duckett, 2015).
  if (isNaN(selectionAsFloat) == false) {
    if (selectionAsFloat > 0) {
      vendor.Funds += selectionAsFloat;
    }
    // This else stateent handles human error inputs that fall below zero
    else {
      console.log("Please enter an amount that is higher than 0".yellow);
      console.log("Press any key to continue".cyan);
      readline.question();
      addMoney();
    }
    // Updates the user on their funds once added
    console.clear();
    console.log(colors.bgCyan("        Your balance now is: £" + vendor.Funds.toFixed(2) + "           ").bold.red)
    console.log("Please press enter to return to the main menu.    ")
    readline.question();
    main_menu();

  } else {
    /*Error prevention and retrieval:if the human error occures and anything but a number is entered, this instructs the user on what to do*/
    console.log("Sorry that was not a number, please try again. Press enter to continue.".yellow);
    readline.question();
    addMoney();
  }
}

/*End of function*/

//Start of function
function creditCheck() {
  console.clear();
  // creditCheck allows user to check funds and retreat to the menu
  console.log(colors.green("Your balance is: " + "£" + vendor.Funds.toFixed(2)));
  console.log("press any key to return to the main menu".cyan);
  readline.question();
  main_menu();
}

//End of function

//Start of function

function refundMoney() {
  console.clear() /**/
  // If statement says that if the balance is <= 0, then there wont be a refund and a easy retreat to the menu
if(vendor.Funds <= 0) {
  console.log("No Refund Available-No balance".cyan)
  console.log("To return to the main menu, press Enter:".cyan)
  readline.question();
  main_menu();
}

//If the user decides to press the key for Money Refund, their balance will show as zero to implement this action to them
else {
  console.log("Select letter for your refund? Y/N".yellow)
  console.log(colors.green("Your balance is: £" + vendor.Funds.toFixed(2)))

  /*selection variable from earlier in the script, is used again for effective usage*/
  var selection = readline.question("Enter Y or N : ".cyan);

  // Switch statement uses toUpperCase function to convert lower case letters to uppercase letters, to
  // ensure and establish all letters corresponding to the options are apprehended and activated successfully
  switch (selection.toUpperCase()) {
    // If Y is pressed, in this case the function empties the funds and zero is prompted to the user, in addition to a
    // menu page retreat
    case "Y":
      console.clear();
      vendor.Funds = 0;
      console.log(colors.cyan("Your balance is now: " + "£" + vendor.Funds.toFixed(2)))
      console.log("To return to the main menu, press Enter".cyan)
      readline.question();
      main_menu();
      break;
      // Alternatively if pressing key N is the case, the user is returned t the menu screen with their funds
    case "N":
      main_menu();
      break;
    default:
      // Error prevention: If the user presses any other key this error message will prompt the user
      console.log("Invalid answer, try again".yellow);
      main_menu();
      break;
  }
 }
}
// End of function
/*Start of program*/
main_menu()

/*Bibliography*/
//Anon., 2018.Logical Operators [ online]. Available from: https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Operators/Logical_Operators [Accessed 20th Nov 2017].
//DUCKETT, J., 2015. Javascript and JQuery. 1st edition. Indeanapolis: Wiley
//W3C., 2018. JavaScript Switch Statement [online]. Available from: https://www.w3schools.com/jsref/jsref_switch.asp [Accessed: 24th Dec 2017].
